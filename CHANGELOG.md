gitlab-prometheus CHANGELOG
===========================

0.2.46
-----
- Fix node_exporter ntpd_metrics log directory.

0.2.25
-----
- Add consul_inventory.
- Fix error raised when `/opt/prometheus/prometheus` doesn't exists on a host at first usage.

0.2.0
-----
- Add influxdb_exporter.

0.1.144
-----
- Restart Prometheus service upon deployment of a new binary.

0.1.143
-----
- Update alertamanger to `0.8.0`.
- Update blackbox_exporter to `0.7.0`.
- Update Prometheus checksum for `1.7.1`.
- Update redis_exporter to `0.11.3`.

0.1.142
-----
- Use file inventory generator for alertmanager targets.

0.1.141
-----
- Remove URL part to fix alertmanager target syntax.

0.1.140
-----
- Add `-web.external-url` flag to Prometheus server.
- Upgrade to Prometheus 1.7.1.
- Use search to populate alertmanager server list for Prometheus.

0.1.130
-----
- upgraded all cookbook versions
- added syntax checker
- changed to vault test fixtures

0.1.127
-----
- Update postgres_exporter to v2.1.
- Add restarts for postgres_exporter binary or config changes.

0.1.126
-----
- Add postgres queries from gitlab-monitor.

0.1.122
-----
- Revert change from `prom_params` to `params` now that we're using string keys for attribute access.
- Update README for `chef_search` option.

0.1.109
-----
- Upgrade to Prometheus 1.6.2, update flags for the new release.

0.1.105
-----
- refactor `params` attribute to `prom_params` to avoid generic cookbook resource collisions

0.1.24
-----
- move from gitlab-cluster-base to base-debian

0.1.23
-----
- add redis_exporter recipe

0.1.22
-----
- standardize prometheus user across all recipes

0.1.21
-----
- add postgres_exporter recipe 

0.1.20
-----
- increase default retention period

0.1.19
-----
- add redis_exporter prometheus_job

0.1.18
-----
- add node_exporter textfile watch path

0.1.17
-----
- change runit services to exec

0.1.16
-----
- fix configuration for ceph_exporter

0.1.15
-----
- update runit scripts to run as the appropriate user

0.1.14
-----
- add ceph_exporter to prometheus jobs

0.1.13
-----
- add ceph_exporter recipe

0.1.12
-----
- remove logging statement

0.1.11
-----
- add node_exporter recipe

0.1.5
-----
- remove unneeded line from blackbox_exporter

0.1.4
-----
- fix blackbox_exporter's use of chef-vault

0.1.3
-----
- changed config option in prometheus 0.20.0 and beyond. The previous only worked because it was deprecated in 0.20.0 and removed later.

0.1.2
-----
- url for prometheus download changed with 1.0.1

0.1.1
-----
- update template for blackbox to use an auth_token variable

0.1.0
-----
- Initial release of gitlab-prometheus
