#
# Cookbook Name GitLab::Monitoring
# Attributes:: prometheus
#
default["prometheus"]["checksum"]    = "9e0e3aeb4ae360ea284b0f751c1a2b40b664ea533ab90af58e8656700db0bc9b"
default["prometheus"]["dir"]         = "/opt/prometheus/prometheus"
default["prometheus"]["alerting_rules_dir"] = "#{node['prometheus']['dir']}/alerts"
default["prometheus"]["recording_rules_dir"] = "#{node['prometheus']['dir']}/recordings"
default["prometheus"]["console_templates_dir"] = "#{node['prometheus']['dir']}/consoles"
default["prometheus"]["inventory_dir"] = "/opt/prometheus/prometheus/inventory"
default["prometheus"]["binary"]      = "#{node['prometheus']['dir']}/prometheus"
default["prometheus"]["log_dir"]     = "/var/log/prometheus/prometheus"
default["prometheus"]["version"]     = "1.8.2"
default["prometheus"]["binary_url"]  = "https://github.com/prometheus/prometheus/releases/download/v#{node['prometheus']['version']}/prometheus-#{node['prometheus']['version']}.linux-amd64.tar.gz"

default["prometheus"]["scrape_interval"] = "15s"
default["prometheus"]["scrape_timeout"] = "10s"
default["prometheus"]["evaluation_interval"] = "15s"

default["prometheus"]["alertmanager"]["port"]            = "9093"
default["prometheus"]["alertmanager"]["inventory"]       = "#{node['prometheus']['dir']}/alertmanagers.yml"

default["prometheus"]["runbooks"]["git_http"]            = "https://gitlab.com/gitlab-com/runbooks.git"
default["prometheus"]["runbooks"]["branch"]              = "master"

default["prometheus"]["flags"]["config.file"]           = "#{node['prometheus']['dir']}/prometheus.yml"
default["prometheus"]["flags"]["web.console.libraries"] = "#{node['prometheus']['dir']}/console_libraries"
default["prometheus"]["flags"]["web.console.templates"] = "#{node['prometheus']['dir']}/consoles"
default["prometheus"]["flags"]["web.external-url"]      = "https://#{node['fqdn']}"

if Gem::Version.new(node["prometheus"]["version"]) < Gem::Version.new("2.0.0")
  default["prometheus"]["flags"]["storage.local.chunk-encoding-version"]   = "2"
  default["prometheus"]["flags"]["storage.local.path"]                     = "#{node['prometheus']['dir']}/data"
  default["prometheus"]["flags"]["storage.local.retention"]                = "8760h0m0s" # 365d
  default["prometheus"]["flags"]["storage.local.series-file-shrink-ratio"] = "0.4"
  default["prometheus"]["flags"]["storage.local.target-heap-size"]         = (node["memory"]["total"].to_i * 0.50 * 1024).to_i.to_s
  default["prometheus"]["rule_files"] = [
    File.join(node["prometheus"]["alerting_rules_dir"], "/*.rules"),
    File.join(node["prometheus"]["recording_rules_dir"], "/*.rules"),
  ]
else
  default["prometheus"]["flags"]["storage.tsdb.path"]      = "#{node['prometheus']['dir']}/data"
  default["prometheus"]["flags"]["storage.tsdb.retention"] = "365d"
  default["prometheus"]["flags"]["storage.tsdb.max-block-duration"] = "7d"
  default["prometheus"]["rule_files"] = [
    File.join(node["prometheus"]["alerting_rules_dir"], "/*.yml"),
    File.join(node["prometheus"]["recording_rules_dir"], "/*.yml"),
  ]
end

default["prometheus"]["install_method"] = "binary"

default["prometheus"]["jobs"] = []
