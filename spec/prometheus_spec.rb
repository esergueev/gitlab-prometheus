require "spec_helper"

describe "gitlab-prometheus::prometheus" do
  context "default execution" do
    before do
      stub_search(:node, "roles:test-role").and_return(
        [{ "fqdn" => "stubbed_node",
           "hostname" => "my.hostname" }]
      )
      stub_search(:node, "recipes:gitlab-prometheus\\:\\:alertmanager").and_return(
        [{ "fqdn" => "stubbed_node",
           "hostname" => "my.hostname",
           "ipaddress" => "10.11.12.13" }]
      )
    end

    context "with a simple chef execution" do
      cached(:chef_run) do
        ChefSpec::ServerRunner.new { |node|
          node.normal["prometheus"]["jobs"]["test-job"] = {
            "target" => ["test-target:8080"],
          }
        }.converge(described_recipe)
      end

      it "syncs the runbooks repo" do
        expect(chef_run).to sync_git("#{Chef::Config[:file_cache_path]}/runbooks").with(
          repository: "https://gitlab.com/gitlab-com/runbooks.git").with(revision: "master")
      end

      it "creates the prometheus dir in the configured location" do
        expect(chef_run).to create_directory("/opt/prometheus/prometheus").with(
          owner: "prometheus",
          group: "prometheus",
          mode: "0755",
          recursive: true
        )
      end

      it "creates the log dir in the configured location" do
        expect(chef_run).to create_directory("/var/log/prometheus/prometheus").with(
          owner: "prometheus",
          group: "prometheus",
          mode: "0755",
          recursive: true
        )
      end

      it "creates the prometheus alerts rules dir in the configured location" do
        expect(chef_run).to create_link("/opt/prometheus/prometheus/alerts").with(
          owner: "prometheus",
          group: "prometheus",
          to: "#{Chef::Config[:file_cache_path]}/runbooks/alerts"
        )
      end

      it "creates the prometheus recording rules dir in the configured location" do
        expect(chef_run).to create_link("/opt/prometheus/prometheus/recordings").with(
          owner: "prometheus",
          group: "prometheus",
          to: "#{Chef::Config[:file_cache_path]}/runbooks/recordings"
        )
      end

      it "creates the prometheus console templates dir in the configured location" do
        expect(chef_run).to create_link("/opt/prometheus/prometheus/consoles").with(
          owner: "prometheus",
          group: "prometheus",
          to: "#{Chef::Config[:file_cache_path]}/runbooks/consoles"
        )
      end

      it "includes runit::default" do
        expect(chef_run).to include_recipe("runit::default")
      end

      it "runs the prometheus service" do
        expect(chef_run).to enable_runit_service("prometheus").with(
          default_logger: true,
          sv_timeout: 120,
          log_dir: "/var/log/prometheus/prometheus"
        )
      end

      it "creates the configuration file with default content" do
        expect(chef_run).to create_file("/opt/prometheus/prometheus/prometheus.yml").with(
          owner: "prometheus",
          group: "prometheus",
          mode: "0644"
        )
        expect(chef_run).to render_file("/opt/prometheus/prometheus/prometheus.yml").with_content { |content|
          expect(content).to eq(<<-eos
---
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 15s
rule_files:
- "/opt/prometheus/prometheus/alerts/*.rules"
- "/opt/prometheus/prometheus/recordings/*.rules"
alerting:
  alertmanagers:
  - file_sd_configs:
    - files:
      - "/opt/prometheus/prometheus/alertmanagers.yml"
scrape_configs:
- job_name: test-job
  static_configs:
  - targets:
    - test-target:8080
eos
                               )
        }
      end

      it "generates a configuration file with jobs in it" do
        expect(chef_run).to render_file("/tmp/prometheus.jobs").with_content { |content|
          expect(content).to include('{"test-job"=>{"target"=>["test-target:8080"]}}')
        }
      end
    end

    context "with relabel configuration" do
      cached(:chef_run_with_relabel) do
        ChefSpec::SoloRunner.new { |node|
          node.normal["prometheus"]["jobs"]["test-job"] = {
            "role_name" => ["test-role"],
            "inventory_file_name" => "test-role",
            "relabel_configs" => "sentinel",
            "exporter_port" => 9100,
          }
          node.normal["prometheus"]["dir"] = "/tmp/prometheus"
        }.converge(described_recipe)
      end

      it "generates a configuration file with jobs and relabel configuration in it" do
        expect(chef_run_with_relabel).to render_file("/tmp/prometheus/inventory/test-role.yml").with_content { |content|
          expect(content).to eq(<<-eos
---
- targets:
  - 192.168.1.2:9100
  labels:
    fqdn: stubbed_node
    instance: stubbed_node:9100
eos
                               )
        }
      end
    end

    context "with relabel configuration" do
      cached(:chef_run_with_relabel) do
        ChefSpec::SoloRunner.new { |node|
          node.normal["prometheus"]["scrape_interval"] = "30s"
          node.normal["prometheus"]["scrape_timeout"] = "30s"
          node.normal["prometheus"]["evaluation_interval"] = "30s"
          node.normal["prometheus"]["jobs"]["test-job"] = {
            "scrape_interval" => "15s",
            "role_name" => ["test-role"],
            "inventory_file_name" => "something",
            "file_inventory": true,
            "public_hosts": ["some-public-host"],
            "relabel_configs" => [
              {
                "source_labels": ["__address__"],
                "regex": "(.*)(:80)?",
                "target_label": "__param_target",
                "replacement": "${1}",
              },
              {
                "target_label": "__address__",
                "replacement": "blackbox.gitlab.com:9115",
              },
            ],
            "exporter_port" => 9100,
          }
        }.converge(described_recipe)
      end

      it "renders the prometheus yaml template" do
        expect(chef_run_with_relabel).to render_file("/opt/prometheus/prometheus/prometheus.yml").with_content { |content|
          expect(content).to eq(<<-eos
---
global:
  scrape_interval: 30s
  scrape_timeout: 30s
  evaluation_interval: 30s
rule_files:
- "/opt/prometheus/prometheus/alerts/*.rules"
- "/opt/prometheus/prometheus/recordings/*.rules"
alerting:
  alertmanagers:
  - file_sd_configs:
    - files:
      - "/opt/prometheus/prometheus/alertmanagers.yml"
scrape_configs:
- job_name: test-job
  scrape_interval: 15s
  relabel_configs:
  - source_labels:
    - __address__
    regex: "(.*)(:80)?"
    target_label: __param_target
    replacement: "${1}"
  - target_label: __address__
    replacement: blackbox.gitlab.com:9115
  honor_labels: true
  file_sd_configs:
  - files:
    - "/opt/prometheus/prometheus/inventory/something.yml"
eos
                               )
        }
      end
    end

    context "with multiple source label rewrite configuration" do
      cached(:chef_run_with_relabel) do
        ChefSpec::SoloRunner.new { |node|
          node.normal["prometheus"]["scrape_interval"] = "30s"
          node.normal["prometheus"]["scrape_timeout"] = "30s"
          node.normal["prometheus"]["evaluation_interval"] = "30s"
          node.normal["prometheus"]["jobs"]["test-job"] = {
            "scrape_interval" => "15s",
            "role_name" => ["test-role"],
            "inventory_file_name" => "something",
            "exporter_port" => 9100,
            "relabel_configs" => [
              {
                "target_label" => "baz",
                "source_labels": %w(foo bar),
              },
            ],
          }
        }.converge(described_recipe)
      end

      it "renders the prometheus yaml template" do
        expect(chef_run_with_relabel).to render_file("/opt/prometheus/prometheus/prometheus.yml").with_content { |content|
          expect(content).to eq(<<-eos
---
global:
  scrape_interval: 30s
  scrape_timeout: 30s
  evaluation_interval: 30s
rule_files:
- "/opt/prometheus/prometheus/alerts/*.rules"
- "/opt/prometheus/prometheus/recordings/*.rules"
alerting:
  alertmanagers:
  - file_sd_configs:
    - files:
      - "/opt/prometheus/prometheus/alertmanagers.yml"
scrape_configs:
- job_name: test-job
  scrape_interval: 15s
  relabel_configs:
  - target_label: baz
    source_labels:
    - foo
    - bar
  honor_labels: true
  file_sd_configs:
  - files:
    - "/opt/prometheus/prometheus/inventory/something.yml"
eos
                               )
        }
      end
    end

    context "with consul inventory" do
      cached(:chef_run_with_consul_inventory) do
        ChefSpec::SoloRunner.new { |node|
          node.normal["prometheus"]["scrape_interval"] = "30s"
          node.normal["prometheus"]["scrape_timeout"] = "30s"
          node.normal["prometheus"]["evaluation_interval"] = "30s"
          node.normal["prometheus"]["jobs"]["test-job"] = {
            "scrape_interval" => "15s",
            "honor_labels" => "false",
            "consul_inventory" => {
              "server" => "localhost:8500",
              "services" => %w(node_exporter cAdvisor),
            },
            "relabel_configs" => [
              {
                "target_label": "dc",
                "source_labels": ["__meta_consul_dc"],
                "regex": "(.*)",
                "replacement": "${1}",
              },
            ],
          }
        }.converge(described_recipe)
      end

      it "renders the prometheus yaml tempalte" do
        expect(chef_run_with_consul_inventory).to render_file("/opt/prometheus/prometheus/prometheus.yml").with_content { |content|
          expect(content).to eq(<<-eos
---
global:
  scrape_interval: 30s
  scrape_timeout: 30s
  evaluation_interval: 30s
rule_files:
- "/opt/prometheus/prometheus/alerts/*.rules"
- "/opt/prometheus/prometheus/recordings/*.rules"
alerting:
  alertmanagers:
  - file_sd_configs:
    - files:
      - "/opt/prometheus/prometheus/alertmanagers.yml"
scrape_configs:
- job_name: test-job
  scrape_interval: 15s
  honor_labels: false
  relabel_configs:
  - target_label: dc
    source_labels:
    - __meta_consul_dc
    regex: "(.*)"
    replacement: "${1}"
  consul_sd_configs:
  - server: localhost:8500
    services:
    - node_exporter
    - cAdvisor
eos
                               )
        }
      end
    end
  end

  context "with a labeled node" do
    before do
      stub_search(:node, "roles:labeled-node").and_return(
        [{ "fqdn" => "labeled.node",
           "hostname" => "labeled.hostname",
           "prometheus" => {
             "labels" => {
               "environment" => "prod",
               "tier" => "fe",
             },
           },
         }]
      )
      stub_search(:node, "recipes:gitlab-prometheus\\:\\:alertmanager").and_return(
        [{ "fqdn" => "stubbed_node",
           "hostname" => "my.hostname",
           "ipaddress" => "10.11.12.13" }]
      )
    end
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal["prometheus"]["jobs"]["test-job"] = {
          "role_name" => ["labeled-node"],
          "inventory_file_name" => "labeled-node",
          "exporter_port" => 9100,
        }
        node.normal["prometheus"]["dir"] = "/tmp/prometheus"
      }.converge(described_recipe)
    end

    it "generates an inventory with a node with additional labels" do
      expect(chef_run).to render_file("/tmp/prometheus/inventory/labeled-node.yml").with_content { |content|
                            expect(content).to eq(<<-eos
---
- targets:
  - 192.168.1.3:9100
  labels:
    fqdn: labeled.node
    environment: prod
    tier: fe
    instance: labeled.node:9100
                    eos
                                                 )
                          }
    end
  end
end
